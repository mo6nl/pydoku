"""
This module shows an SuDoku board and solves this in discrete steps.

The solving algorithm determines in each iteration for every cell the
possible solutions. If a cell had one solution this is filled in and shown
on the board.

Note that this algorithm only works for "easy" SoDokus where there is at
least one cell available with only one possible solution in every iteration.
More difficult SoDokus require a more elaborate strategy.
"""

from Tkinter import Tk, Canvas
import random

# Global constants.
SQUARE_SIZE = 50
GUTTER = 10
BOARD_SIZE = 2 * GUTTER + 9 * SQUARE_SIZE + 2
FONT_FACE = 'Courier'
FONT_SIZE = SQUARE_SIZE / 2


def sudoku_square(canvas, square, pos, number):
    """
    Draws a SuDoku square given the square coordinates and the position.
    """
    grid_x1 = GUTTER + (3 * square[0] + pos[0]) * SQUARE_SIZE + square[0]
    grid_y1 = GUTTER + (3 * square[1] + pos[1]) * SQUARE_SIZE + square[1]
    grid_x2 = GUTTER + (3 * square[0] + pos[0] + 1) * SQUARE_SIZE + square[0]
    grid_y2 = GUTTER + (3 * square[1] + pos[1] + 1) * SQUARE_SIZE + square[1]
    canvas.create_rectangle(grid_x1, grid_y1, grid_x2, grid_y2)
    if number > 0:
        canvas.create_text(grid_x1 + SQUARE_SIZE / 2,
                           grid_y1 + SQUARE_SIZE / 2,
                           text=number, font=(FONT_FACE, FONT_SIZE))


def draw_sudoku(canvas, sudoku):
    """
    Draw a wholy SuDoku board by drawing all individual cells.
    """
    for x_square in range(0, 3):
        for y_square in range(0, 3):
            for x_pos in range(0, 3):
                for y_pos in range(0, 3):
                    number = sudoku[y_square * 3 + y_pos][x_square * 3 + x_pos]
                    square = [x_square, y_square]
                    pos = [x_pos, y_pos]
                    sudoku_square(canvas, square, pos, number)


def random_update_sudoku(window, canvas, sudoku):
    """
    Function to test the drawing of the board by randomly filling in cells.
    """
    y_square = random.randrange(3)
    x_square = random.randrange(3)
    y_pos = random.randrange(3)
    x_pos = random.randrange(3)
    x_array = 3 * x_square + x_pos
    y_array = 3 * y_square + y_pos
    print("x_square = %s, y_square = %s, x_pos = %s, y_pos = %s" %
          (x_square, y_square, x_pos, y_pos))
    if sudoku[y_array][x_array] == 0:
        number = random.randrange(9) + 1
        sudoku[y_array][x_array] = number
        square = [x_square, y_square]
        pos = [x_pos, y_pos]
        sudoku_square(canvas, square, pos, number)
    window.after(100, random_update_sudoku, window, canvas, sudoku)


def find_sudoku_possibilities(x_square, y_square, x_pos, y_pos, sudoku):
    """
    Given a square x and y, and the position within the square,
    find the possible numbers and return a list with values.
    """
    x_array = 3 * x_square + x_pos
    y_array = 3 * y_square + y_pos
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    # Find all numbers in row.
    row_numbers = []
    for i in range(0, 9):
        if sudoku[y_array][i] != 0:
            row_numbers.append(sudoku[y_array][i])
            if sudoku[y_array][i] in numbers:
                numbers.remove(sudoku[y_array][i])

    # Find all numbers in column.
    col_numbers = []
    for i in range(0, 9):
        if sudoku[i][x_array] != 0:
            col_numbers.append(sudoku[i][x_array])
            if sudoku[i][x_array] in numbers:
                numbers.remove(sudoku[i][x_array])

    # Find all numbers in square.
    square_numbers = []
    for i in range(0, 3):
        for j in range(0, 3):
            if sudoku[j + y_square * 3][i + x_square * 3] != 0:
                square_numbers.append(
                    sudoku[j + y_square * 3][i + x_square * 3])
                if sudoku[j + y_square * 3][i + x_square * 3] in numbers:
                    numbers.remove(sudoku[j + y_square * 3][i + x_square * 3])

    return numbers


def solve_sudoku_block(canvas, x_square, y_square, sudoku):
    """
    Solve a block using an easy algorithm.

    :return: Number of cells solved.
    """
    found = 0
    for x_pos in range(0, 3):
        for y_pos in range(0, 3):
            x_array = 3 * x_square + x_pos
            y_array = 3 * y_square + y_pos
            if sudoku[y_array][x_array] == 0:
                numbers = find_sudoku_possibilities(x_square, y_square,
                                                    x_pos, y_pos, sudoku)
                if len(numbers) == 1:
                    sudoku[y_array][x_array] = numbers[0]
                    square = [x_square, y_square]
                    pos = [x_pos, y_pos]
                    sudoku_square(canvas, square, pos, numbers[0])
                    found = found + 1
    return found


def solve_sudoku(window, canvas, sudoku):
    """
    This function tries to solve (easy) SoDokus by iterating over all cells
    and finding the cells which have only one solution.
    """
    found = 0
    for x_square in range(0, 3):
        for y_square in range(0, 3):
            found = found + solve_sudoku_block(canvas, x_square, y_square,
                                               sudoku)
    print "Found: %s" % found
    if found > 0:
        window.after(1000, solve_sudoku, window, canvas, sudoku)


# Define sudoku datastructures.

# Daily SuDoku 20170715 - Easy
SUDOKU_EASY = [
    [0, 0, 0, 1, 0, 5, 0, 6, 8],
    [0, 0, 0, 0, 0, 0, 7, 0, 1],
    [9, 0, 1, 0, 0, 0, 0, 3, 0],

    [0, 0, 7, 0, 2, 6, 0, 0, 0],
    [5, 0, 0, 0, 0, 0, 0, 0, 3],
    [0, 0, 0, 8, 7, 0, 4, 0, 0],

    [0, 3, 0, 0, 0, 0, 8, 0, 5],
    [1, 0, 5, 0, 0, 0, 0, 0, 0],
    [7, 9, 0, 4, 0, 1, 0, 0, 0],
]

# Daily SuDoku 20170713 - Hard
SUDOKU_HARD = [
    [0, 0, 0, 6, 0, 3, 0, 0, 0],
    [0, 9, 4, 0, 0, 0, 0, 0, 1],
    [6, 8, 0, 0, 1, 0, 0, 0, 0],

    [0, 0, 6, 5, 0, 0, 0, 9, 8],
    [0, 0, 8, 0, 2, 0, 5, 0, 0],
    [4, 5, 0, 0, 0, 6, 1, 0, 0],

    [0, 0, 0, 0, 5, 0, 0, 2, 9],
    [5, 0, 0, 0, 0, 0, 8, 1, 0],
    [0, 0, 0, 2, 0, 7, 0, 0, 0],
]


def solve():
    """
    Solve the SuDoku board.
    """
    # TK initialization.
    tk_win = Tk()
    tk_win.winfo_toplevel().title("SuDoku solver in Python")

    # Focus the windows, e.g. make it visible.
    tk_win.lift()
    tk_win.attributes("-topmost", True)

    # Canvas initialization.
    canvas_board = Canvas(tk_win, width=BOARD_SIZE, height=BOARD_SIZE)
    canvas_board.pack()

    # Initialize the sudoku board.
    sudoku = SUDOKU_EASY

    # Draw total SuDoku field with initial values.
    draw_sudoku(canvas_board, sudoku)

    # Schedule a random redraw.
    # tk_win.after(3 * 1000, random_update_sudoku,
    #              tk_win, canvas_board, sudoku)

    # Schedule a solve task after 3 seconds.
    tk_win.after(3 * 1000, solve_sudoku,
                 tk_win, canvas_board, sudoku)

    # Start main execution loop for UI.
    tk_win.mainloop()


# Start main function.
solve()
