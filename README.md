# Pydoku

My first steps into programming with Python: creating a SuDoku solver.

This project holds two directories:

- `1-simple`: a procedural Python program visually displaying a simple SuDoku solving proces.
- `2-reorganised`: an OO-style refactored setup with a Python package doing the same as the `1-simple` program

The code in this project is validated using `pylint` and `pycodestyle` and conforms to the PEP-8 code style.

## References

### PEP: Python Enhancement Proposals

#### PEP-8 Python Code Style
- https://www.python.org/dev/peps/pep-0008/

#### PEP-8 Docstring convention
- https://www.python.org/dev/peps/pep-0257/

## Code style tests

pycodestyle is a tool to check your Python code against some of the style conventions in PEP 8.
See: https://pypi.org/project/pycodestyle/

pylint analyzes Python source code looking for bugs and signs of poor quality.
See: https://www.logilab.org/857

### Install pystyle

```bash
$ pip install pycodestyle
```

### Usage

Basic:

```bash
$ pycodestyle --first 1-simple/visual_solve.py
```

Show details:

```bash
$ pycodestyle --show-source --show-pep8 1-simple/visual_solve.py
```

### Install pylint

```bash
$ pip install pylint
```

### Usage

```bash
$ pylint 1-simple/visual_solve.py
```

### Project file layout

#### "Modern Package Template"
- https://pypi.org/project/modern-package-template/
- `pip install modern-package-template`
