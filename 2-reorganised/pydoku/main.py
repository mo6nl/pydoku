"""
SuDoku solver in Python, OO-style version.
"""

from solver import solver

# Daily SuDoku 20170715 - Easy
SUDOKU_EASY = [
    [0, 0, 0, 1, 0, 5, 0, 6, 8],
    [0, 0, 0, 0, 0, 0, 7, 0, 1],
    [9, 0, 1, 0, 0, 0, 0, 3, 0],

    [0, 0, 7, 0, 2, 6, 0, 0, 0],
    [5, 0, 0, 0, 0, 0, 0, 0, 3],
    [0, 0, 0, 8, 7, 0, 4, 0, 0],

    [0, 3, 0, 0, 0, 0, 8, 0, 5],
    [1, 0, 5, 0, 0, 0, 0, 0, 0],
    [7, 9, 0, 4, 0, 1, 0, 0, 0],
]

SOLVER = solver.SudokuSolver(SUDOKU_EASY)
SOLVER.start_solve()
