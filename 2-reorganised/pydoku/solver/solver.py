"""
SuDoku solver controller class.
"""

from Tkinter import Tk, Canvas
from .board import board
from .board.state import PyDokuState


class SudokuSolver(object):
    """
    SuDoku solver class.
    """

    def __init__(self, sudoku):
        # Store sudoku datastructure.
        self.sudoku_state = PyDokuState()
        self.sudoku_state.init_from_array(sudoku)

        # Initialize class variables.
        self.tk_win = None
        self.sudoku_view = None

    def start_solve(self):
        """
        Start the solving of the SuDoku visually showing progress.
        """

        # TK initialization.
        self.tk_win = Tk()
        self.tk_win.winfo_toplevel().title("SuDoku solver in Python")

        # Focus the windows, e.g. make it visible.
        self.tk_win.lift()
        self.tk_win.attributes("-topmost", True)

        # Canvas initialization.
        canvas_board = Canvas(self.tk_win, width=board.BOARD_SIZE,
                              height=board.BOARD_SIZE)
        canvas_board.pack()

        self.sudoku_view = board.PyBoard(canvas_board)
        self.sudoku_view.draw_sudoku(self.sudoku_state)

        # Schedule a solve task after 3 seconds.
        self.tk_win.after(3 * 1000, self.solve_sudoku)

        # Start main execution loop for UI.
        self.tk_win.mainloop()

    def find_sudoku_possibilities(self, square, pos):
        """
        Given a square x and y, and the position within the square,
        find the possible numbers and return a list with values.
        """
        numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        # Find all numbers in row.
        for i in range(0, 3):
            for j in range(0, 3):
                row_square = [i, square[1]]
                row_pos = [j, pos[1]]
                cell_numbers = self.sudoku_state.get_numbers(row_square,
                                                             row_pos)
                if len(cell_numbers) == 1:
                    number = cell_numbers[0]
                    if number in numbers:
                        numbers.remove(number)

        # Find all numbers in column.
        for i in range(0, 3):
            for j in range(0, 3):
                col_square = [square[0], i]
                col_pos = [pos[0], j]
                cell_numbers = self.sudoku_state.get_numbers(col_square,
                                                             col_pos)
                if len(cell_numbers) == 1:
                    number = cell_numbers[0]
                    if number in numbers:
                        numbers.remove(number)

        # Find all numbers in square.
        for i in range(0, 3):
            for j in range(0, 3):
                col_square = [square[0], square[1]]
                col_pos = [i, j]
                cell_numbers = self.sudoku_state.get_numbers(col_square,
                                                             col_pos)
                if len(cell_numbers) == 1:
                    number = cell_numbers[0]
                    if number in numbers:
                        numbers.remove(number)

        return numbers

    def solve_sudoku_block(self, x_square, y_square):
        """
        Solve a block using an easy algorithm.

        :return: Number of cells solved.
        """
        found = 0
        for x_pos in range(0, 3):
            for y_pos in range(0, 3):
                square = [x_square, y_square]
                pos = [x_pos, y_pos]
                numbers = self.sudoku_state.get_numbers(square, pos)
                if len(numbers) > 1:
                    numbers = self.find_sudoku_possibilities(square, pos)
                    if len(numbers) == 1:
                        self.sudoku_state.set_numbers(square, pos,
                                                      [numbers[0]])
                        self.sudoku_view.sudoku_square(square, pos, numbers[0])
                        found = found + 1
        return found

    def solve_sudoku(self):
        """
        This function tries to solve (easy) SoDokus by iterating over all cells
        and finding the cells which have only one solution.
        """
        found = 0
        for x_square in range(0, 3):
            for y_square in range(0, 3):
                found = found + self.solve_sudoku_block(x_square, y_square)
        print "Found: %s" % found
        if found > 0:
            self.tk_win.after(1000, self.solve_sudoku)
