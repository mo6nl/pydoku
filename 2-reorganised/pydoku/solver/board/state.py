"""
SuDoku board model class.
"""


class PyDokuState(object):
    """
    SuDoku board model class holding possible solutions.
    """

    def __init__(self):
        self.sudoku = [list()] * 9
        for row in range(0, 9):
            row_list = [list()] * 9
            self.sudoku[row] = row_list

        for x_square in range(0, 3):
            for y_square in range(0, 3):
                for x_pos in range(0, 3):
                    for y_pos in range(0, 3):
                        self.sudoku[y_square*3 + y_pos][x_square*3 + x_pos] = \
                            range(1, 9)

    def init_from_array(self, sudoku_array):
        """
        Initialize board datastructure from multi dimensional list.
        :param sudoku_array: A SuDoku representation of 9 rows with 9 cols.
        """
        for x_square in range(0, 3):
            for y_square in range(0, 3):
                for x_pos in range(0, 3):
                    for y_pos in range(0, 3):
                        x_index = x_square * 3 + x_pos
                        y_index = y_square * 3 + y_pos
                        value = sudoku_array[y_index][x_index]
                        if value > 0:
                            self.sudoku[y_index][x_index] = [value]

    def get_numbers(self, square, pos):
        """
        Returns an array with possible solutions for the given element.
        :param square: A list holding x and y of the block.
        :param pos: A list holding x and y of the element within a block.
        :return: An list holding the possible values for the given element.
        """
        return self.sudoku[square[1] * 3 + pos[1]][square[0] * 3 + pos[0]]

    def set_numbers(self, square, pos, numbers):
        """
        Sets the possible solutions for a given element.
        :param square: A list holding x and y of the block.
        :param pos: A list holding x and y of the element within a block.
        :param numbers: An list holding the possible values.
        """
        self.sudoku[square[1] * 3 + pos[1]][square[0] * 3 + pos[0]] = numbers
