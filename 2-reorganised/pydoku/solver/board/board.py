"""
SuDoku board view class.
"""

# Global constants.
SQUARE_SIZE = 50
GUTTER = 10
BOARD_SIZE = 2 * GUTTER + 9 * SQUARE_SIZE + 2
FONT_FACE = 'Courier'
FONT_SIZE = SQUARE_SIZE / 2


class PyBoard(object):
    """
    SuDoku board class.
    """

    def __init__(self, canvas):
        self.canvas = canvas

    def sudoku_square(self, square, pos, number):
        """
        Draws a SuDoku square given the square coordinates and the position.
        """
        grid_x1 = GUTTER + (3 * square[0] + pos[0]) * SQUARE_SIZE + \
            square[0]
        grid_y1 = GUTTER + (3 * square[1] + pos[1]) * SQUARE_SIZE + \
            square[1]
        grid_x2 = GUTTER + (3 * square[0] + pos[0] + 1) * SQUARE_SIZE + \
            square[0]
        grid_y2 = GUTTER + (3 * square[1] + pos[1] + 1) * SQUARE_SIZE + \
            square[1]
        self.canvas.create_rectangle(grid_x1, grid_y1, grid_x2, grid_y2)
        if number > 0:
            self.canvas.create_text(grid_x1 + SQUARE_SIZE / 2,
                                    grid_y1 + SQUARE_SIZE / 2,
                                    text=number, font=(FONT_FACE, FONT_SIZE))

    def draw_sudoku(self, sudoku_state):
        """
        Draw a whole SuDoku board by drawing all individual cells.

        :param sudoku_state: A PyDokuState object.
        """
        for x_square in range(0, 3):
            for y_square in range(0, 3):
                for x_pos in range(0, 3):
                    for y_pos in range(0, 3):
                        square = [x_square, y_square]
                        pos = [x_pos, y_pos]
                        numbers = sudoku_state.get_numbers(square, pos)
                        number = 0
                        if len(numbers) == 1:
                            number = numbers[0]
                        self.sudoku_square(square, pos, number)
